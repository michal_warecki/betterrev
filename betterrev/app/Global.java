import java.util.concurrent.TimeUnit;

import play.Application;
import play.GlobalSettings;
import play.libs.Akka;
import scala.concurrent.duration.Duration;
import update.GithubPoller;


public class Global extends GlobalSettings {
	
	@Override
	public void onStart(Application app) {
//		Akka.system()
//			.scheduler()
//			.schedule(Duration.Zero(),
//					  Duration.create(5, TimeUnit.MINUTES),
//					  new GithubPoller(),
//					  Akka.system().dispatcher());
		
		// Temporarily running once to avoid github rate limits
		new GithubPoller().run();
	}
	
}
