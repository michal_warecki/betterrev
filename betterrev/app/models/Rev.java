package models;

import static update.ProjectConfiguration.getOwner;
import static update.ProjectConfiguration.getProject;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;
import update.ProjectConfiguration;

@Entity
public class Rev extends Model {

	private static final long serialVersionUID = 188525469548289315L;

	public static Finder<Long, Rev> objects = new Finder<>(Long.class, Rev.class);

	@Id
	public Long id;

	@Required
	public String name;
	
	public Rev() {}

	public Rev(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public String githubWebpage() {
		return String.format("https://github.com/%s/%s/pull/%d", getOwner(), getProject(), id);
	}

}
