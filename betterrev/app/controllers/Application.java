package controllers;

import models.Rev;
import play.*;
import play.mvc.*;

import views.html.*;

public class Application extends Controller {
  
    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }
    
    public static Result revs() {
    	return ok(revs.render(Rev.objects.all()));
    }
    
    public static Result rev(Long id) {
        throw new UnsupportedOperationException();
    }

}
