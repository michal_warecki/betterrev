package update;

import static update.ProjectConfiguration.getOwner;
import static update.ProjectConfiguration.getProject;

import java.io.IOException;
import java.util.List;

import models.Rev;

import org.eclipse.egit.github.core.PullRequest;
import org.eclipse.egit.github.core.RepositoryId;
import org.eclipse.egit.github.core.client.GitHubClient;
import org.eclipse.egit.github.core.service.PullRequestService;

import play.Logger;

/**
 * Polls github in order
 * 
 * @author richard
 *
 */
public class GithubPoller implements Runnable {
	
	private final RepositoryId repository;
	private final PullRequestService pullLookup;
	
	public GithubPoller() {
		repository = new RepositoryId(getOwner(), getProject());
		pullLookup = new PullRequestService();
	}

	@Override
	public void run() {
		Logger.info("Polling github");
		try {
			for (PullRequest request : pullLookup.getPullRequests(repository, "open")) {
				long id = request.getId();
				Rev rev = Rev.objects.byId(id);
				if (rev == null) {
					rev = new Rev(id, request.getTitle());
					rev.save();
				}
			}
		} catch (Throwable e) {
			Logger.error("Error polling github", e);
		}
	}

}
