# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table rev (
  id                        bigint not null,
  name                      varchar(255),
  constraint pk_rev primary key (id))
;

create sequence rev_seq;




# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists rev;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists rev_seq;

